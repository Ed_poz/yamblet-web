var dates_desc = {
    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["January", "February", "March", "April", "May", "June", "July", "August", "Septiembre", "October", "November", "December"],
    monthsShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "dd - M - yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};

function lanza_busqueda(e, id_busqueda){

   if ((e.type == 'keypress' && e.keyCode == 13) || e.type == 'click') {

       var busqueda = $("#"+id_busqueda).val();

       if (busqueda != '') {
            window.location = "./?view=resultados&b=" + busqueda;
       }

    }

}

function datetime_to_yyyymmdd(datetime){
    var dia = datetime.getDate().toString().length == 1 ? '0' + datetime.getDate().toString() : datetime.getDate().toString();
    var mes = datetime.getMonth() + 1;
    mes = mes.toString().length == 1 ? '0' + mes.toString() : mes.toString();
    var anhio = datetime.getFullYear()

    return anhio + mes + dia;
}

function yyyymmdd_to_yyyymmdd(yyyymmdd){
    var dia = yyyymmdd.substring(6, 8);
    var mes = yyyymmdd.substring(4, 6);
    var anhio = yyyymmdd.substring(0, 4);

    return anhio + "-" + mes + "-" + dia;
}

function yyyymmdd_to_ddMyyyy(yyyymmdd){
    var dia = yyyymmdd.substring(6, 8);
    var mes = yyyymmdd.substring(4, 6);
    var anhio = yyyymmdd.substring(0, 4);

    var mes_short = dates_desc.monthsShort[parseInt(mes)-1];

    return dia + " - " + mes_short + " - " + anhio;
}

function yyyymmdd_to_datetime(yyyymmdd){
    var dia = parseInt(yyyymmdd.substring(6, 8));
    var mes = parseInt(yyyymmdd.substring(4, 6)) - 1;
    var anhio = parseInt(yyyymmdd.substring(0, 4));


    return new Date(anhio, mes, dia);
}


function custom_notify(tipo, titulo, mensaje) {
	$.Notification.autoHideNotify(
		tipo,
		'top right', 
		titulo, 
		mensaje
	);
}

function custom_alert(titulo, texto, tipo, callback) {
    swal({
        title: titulo,
        text: texto,
        type: tipo,
        showCancelButton: true,
        confirmButtonColor: '#4fa7f3',
        cancelButtonColor: '#d57171',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        timer: 5000
    }).then(
    function () {
    	callback();
    	console.log("Sweet Alert Aceptar");
    },
    function (dismiss) {
    if (dismiss === 'timer') {
      console.log('I was closed by the timer')
    }
  })
}

function modal_prevent_closing() {
    $('.modal-prevent-close').on('hide.bs.modal', function (e) {
        console.log("Modal cerrandose");
        console.log(e);

        if ($(e.target).attr('id') != $(e.currentTarget).attr('id')) { //para el caso del datepicker que disparaba este evento
            return;
        }

        var this_modal = $(this);
        if (this_modal.attr("closing") == "force_closing") {
            this_modal.attr("closing", "");
            return;
        }
        e.preventDefault();
        e.stopImmediatePropagation();
        var close_modal = function(){
            this_modal.attr("closing", "force_closing");
            this_modal.modal('hide');
        }
        custom_alert("Atención", "Si cierra la ventana perderá todos los cambios <br> ¿Seguro que desea continuar?", "question", close_modal);

        return false;
    });
}

