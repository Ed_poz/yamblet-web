<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="assets/images/backgrounds/02.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto ml-auto">
                <a class="navbar-brand" href="#">
                    <span style="color: #B76DCB; font-size: 25px;"> YAMBLET <span>
                </a>
            </li>
            <li class="nav-item d-md-none">
                <a class="nav-link close-navbar">
                    <i class="ft-x"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="navigation-background"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li>
                <a href="?" class="waves-effect">
                    <i class="ft-layout"></i>
                    <span>Películas</span>
                    <span class="menu-arrow"></span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu -->
