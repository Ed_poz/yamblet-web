app.controller("controller_detalles", ['$scope', '$compile', '$window', '$http','$document', function($scope,$compile,$window,$http,$document){

	$scope.obj_starship = {};
	var opcion_guardar;

 	$scope.init = function () {
 		$scope.llena_campos();
 	}

 	$scope.llena_campos = function (){
 		var http_request = {
 			method: 'GET',
 			url: starwars_api + "/starships/" + id
 		};
 		$http(http_request).then(function(response) {
 			if(response) { 
 				$scope.obj_starship = response.data;
 			} else  {
 				console.log("No hay datos");
 			}
 		}, function(response) {
 			console.log("Error al generar petición: " + response);
 		});
 		$scope.validar_db(id);
 	}

 	$scope.validar_db = function(id) {
 		var http_request = {
 			method: 'GET',
 			url: global_apiserver + "/naves/get_by_id/?id="+id
 		}
 		$http(http_request).then(function(response){
 			if(response.data!="no hay datos"){
 				$scope.obj_starship.hyperdrive_rating=response.data.HIPERVELOCIDAD;
 				$scope.obj_starship.cost_in_credits=response.data.COSTO;
 				$scope.obj_starship.max_atmosphering_speed=response.data.MAX_VELOCIDAD;
 				opcion_guardar = "editar"
 			} else {
 				console.log("No hay datos");
 				opcion_guardar = "guardar";
 			}
 		});
 	}

 	$scope.btn_guardar = function(){
 		console.log($scope.obj_starship);
 		if (opcion_guardar == 'guardar') {
	    	var http_request = {
	       		method: 'POST',
	        	url: global_apiserver + "/naves/insert_nave/?id="+id,
	        	data: angular.toJson($scope.obj_starship)
	     	};
	    } else if (opcion_guardar == 'editar') {
	     	var http_request = {
	        	method: 'POST',
	        	url: global_apiserver + "/naves/update_nave/?id="+id,
	        	data: angular.toJson($scope.obj_starship),
	      	};
	    } else {
	    	console.log("Se debe definir una opcion para guardar");
	    }
	    $http(http_request).then(function(response) {
	     	if(response.data) { 
	        	if (response.data.resultado == "ok") {
	           		swal(
						'¡Insertar!',
						'Se ha insertado el registro con éxito.',
						'success'
					)
					window.location.reload();
	        	} else if (response.data.resultado == "invalido"){
		        	swal(
						'¡Insertar!',
						'No se ha podido insertar el registro con éxito.',
						'success'
					)
		          	console.log(response.data);
	        	} else if (response.data.resultado == "error"){
		        }
		    } else {
		        console.log("No hay datos");
		    }
		},function(response) {
	      console.log("Error al generar petición: " + response);
		});
 	}

 }]);