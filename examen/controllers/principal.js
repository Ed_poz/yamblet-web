app.controller("controller_principal", ['$scope', '$compile', '$window', '$http','$document', function($scope,$compile,$window,$http,$document){

	$scope.obj_starwars = {};
	$scope.busqueda = {};

 	$scope.init = function () {
 		$scope.llena_tabla_articulos();
 	}

 	$scope.llena_tabla_articulos = function (){
 		var http_request = {
 			method: 'GET',
 			url: starwars_api + "/films/"
 		};
 		$http(http_request).then(function(response) {
 			if(response) { 
 				$scope.obj_starwars = response.data.results;
 				for (var i = 0; i < $scope.obj_starwars.length; i++) {
 					$scope.obj_starwars[i].ID=$scope.obj_starwars[i].url.split("/")[5];
 				}
 			} else  {
 				console.log("No hay datos");
 			}
 		}, function(response) {
 			console.log("Error al generar petición: " + response);
 		});
 	}

 	$scope.naves = function(id) {
 		window.location= "?view=naves&id="+id;
 	}

 }]);