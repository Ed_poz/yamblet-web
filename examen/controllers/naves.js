app.controller("controller_naves", ['$scope', '$compile', '$window', '$http','$document', function($scope,$compile,$window,$http,$document){

	$scope.obj_peliculas = {};
	$scope.obj_starships = [];
	$scope.busqueda = {};

 	$scope.init = function () {
 		$scope.llena_tabla_articulos();
 	}

 	$scope.llena_tabla_articulos = function (){
 		var http_request = {
 			method: 'GET',
 			url: starwars_api + "/films/"+id
 		};
 		$http(http_request).then(function(response) {
 			if(response) { 
 				$scope.obj_peliculas = response.data.starships; 				
 				$scope.starships();
 			} else  {
 				console.log("No hay datos");
 			}
 		}, function(response) {
 			console.log("Error al generar petición: " + response);
 		});
 	}

 	$scope.starships = function(){
 		for (var i = 0; i < $scope.obj_peliculas.length; i++) {
			var http_request = {
	 			method: 'GET',
	 			url: $scope.obj_peliculas[i]
 			};
 			$http(http_request).then(function(response) {
				if(response) { 
					$scope.obj_starships.push(response.data);
				} else  {
					console.log("No hay datos");
				}
 			});
		}
 	}

 	$scope.details = function(url) {
 		id=url.split("/")[5]
 		window.location= "?view=detalles&id="+id;
 	}

 }]);