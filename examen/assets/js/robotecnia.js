function robotecnia_notify(tipo, titulo, mensaje) {
	$.Notification.autoHideNotify(
		tipo,
		'top right', 
		titulo, 
		mensaje
	);
}

function robotecnia_alert(titulo, texto, tipo, callback) {
    swal({
        title: titulo,
        text: texto,
        type: tipo,
        showCancelButton: true,
        confirmButtonColor: '#4fa7f3',
        cancelButtonColor: '#d57171',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        timer: 5000
    }).then(
    function () {
    	callback();
    	console.log("Sweet Alert Aceptar");
    },
    function (dismiss) {
    if (dismiss === 'timer') {
      console.log('I was closed by the timer')
    }
  })
}