<script type="text/javascript">
    <?php
    $id = $_REQUEST["id"];
    echo "var id='" . $id . "';";
    ?>
</script>

<div class="app-content content" ng-controller="controller_detalles" ng-init="init();">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title">Detalles de nave</h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="?view=">Peliculas</a>
                            </li>
                            <li class="breadcrumb-item"><a href="?">Naves</a>
                            </li>
                            <li class="breadcrumb-item active"><a href="#">Detalles</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Lista de detalles de la nave seleccionada</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 15px;"><span> Datos primarios </span></div>
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">Nombre:</label>
                                            <span ng-bind="obj_starship.name"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">MGLT:</label>
                                            <span ng-bind="obj_starship.MGLT"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">Pasajeros:</label>
                                            <span ng-bind="obj_starship.passengers"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group"> 
                                            <label class="control-label">Hecho por:</label>
                                            <span ng-bind="obj_starship.manufacturer"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">Capacidad de carga:</label>
                                            <span ng-bind="obj_starship.cargo_capacity"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">Hipervelocidad:</label><br>
                                            <input type="text" min="0" class="form-control" ng-model="obj_starship.hyperdrive_rating" placeholder="Hipervelocidad" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">Costo:</label><br>
                                            <input type="text" min="0" class="form-control" ng-model="obj_starship.cost_in_credits" placeholder="Costo" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"> 
                                            <label class="control-label">Máxima velocidad atmósferica:</label><br>
                                            <input type="text" min="0" class="form-control" ng-model="obj_starship.max_atmosphering_speed" placeholder="Máxima velocidad atmósferica" required> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row pull-right">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-min-width mr-1 mb-1" ng-click="btn_guardar()">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>