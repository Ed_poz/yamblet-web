<script type="text/javascript">
<?php
    $id = $_REQUEST["id"];
    echo "var id = '" . $id . "'; ";
?>
</script>

<!-- Table head options with primary background start -->
<div class="app-content content" ng-controller="controller_naves" ng-init="init();">
	<div class="content-wrapper">
		<div class="content-wrapper-before"></div>
		<div class="content-header row">
			<div class="content-header-left col-md-4 col-12 mb-2">
				<h3 class="content-header-title">Películas</h3>
			</div>
			<div class="content-header-right col-md-8 col-12">
				<div class="breadcrumbs-top float-md-right">
					<div class="breadcrumb-wrapper mr-1">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="?view=">Peliculas</a>
							</li>
							<li class="breadcrumb-item"><a href="?view=naves">Naves</a>
                            </li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="content-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Navega por el listado de naves por película de StarWars</h4>
							<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>							
						</div>
						<div class="card-content collapse show">
							<div class="card-body">
								<p>En esta sección podrás buscar, filtrar y seleccionar todas las naves que aparecen en la película seleccionada de StarWars</p>
							</div>
							<form class="form form-horizontal">
								<div class="form-body"> 
									<div class="col-md-12"> 
										<div class="form-group">
											<div class="row">
												<div class="col-md-8">
													<h5>Puedes filtrar por:</h5>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<h6>Nombre</h6>
													<input type="text" class="form-control" ng-model="busqueda.name">
												</div>
												<div class="col-md-4">
													<h6>Numero de pasajeros</h6>
													<input type="text" class="form-control" ng-model="busqueda.passengers">
												</div>
											</div>
										</div> 
									</div>
								</div>
							</form>
							<div class="table-responsive">
								<table class="table">
									<thead class="bg-primary white">
										<tr>
											<th width="30%">Nombre</th> 
											<th width="20%">Hecho por</th>
											<th width="20%">Costo</th>
											<th width="20%">Velocidad máxima de atmósfera</th>
											<th width="10%">Pasajeros</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="naves in obj_starships | filter:busqueda" ng-click="details(naves.url);">
											<td>{{ naves.name}}</td> 
											<td>{{ naves.manufacturer }}</td>             
											<td>{{ naves.cost_in_credits }}</td> 
											<td>{{ naves.max_atmosphering_speed }}</td> 
											<td>{{ naves.passengers}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Table head options with primary background end -->
