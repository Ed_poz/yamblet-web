<!-- Table head options with primary background start -->
<div class="app-content content" ng-controller="controller_principal" ng-init="init();">
	<div class="content-wrapper">
		<div class="content-wrapper-before"></div>
		<div class="content-header row">
			<div class="content-header-left col-md-4 col-12 mb-2">
				<h3 class="content-header-title">Películas</h3>
			</div>
			<div class="content-header-right col-md-8 col-12">
				<div class="breadcrumbs-top float-md-right">
					<div class="breadcrumb-wrapper mr-1">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="?view=">Peliculas</a>
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="content-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Navega por el listado de películas de StarWars</h4>
							<a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>							
						</div>
						<div class="card-content collapse show">
							<div class="card-body">
								<p>En esta sección podrás buscar, filtrar y seleccionar todas las películas de StarWars</p>
							</div>
							<form class="form form-horizontal">
								<div class="form-body"> 
									<div class="col-md-12"> 
										<div class="form-group">
											<div class="row">
												<div class="col-md-8">
													<h5>Puedes filtrar por:</h5>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<h6>Nombre</h6>
													<input type="text" class="form-control" ng-model="busqueda.title">
												</div>
												<div class="col-md-4">
													<h6>Productor</h6>
													<input type="text" class="form-control" ng-model="busqueda.producer">
												</div>
											</div>
										</div> 
									</div>
								</div>
							</form>
							<div class="table-responsive">
								<table class="table">
									<thead class="bg-primary white">
										<tr>
											<th width="30%">Titulo</th> 
											<th width="20%">Director</th>
											<th width="20%">Productor</th>
											<th width="20%">Fecha de lanzamiento</th>
											<th width="10%"></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="peliculas in obj_starwars | filter:busqueda">
											<td>{{ peliculas.title }} episodio {{ peliculas.episode_id}}</td> 
											<td>{{ peliculas.director }}</td>             
											<td>{{ peliculas.producer }}</td> 
											<td>{{ peliculas.release_date }}</td> 
											<td>
												<button class="btn btn-primary btn-sm" type="button" ng-click="naves(peliculas.ID);">
													<a><font color="white">Ver naves</font></a>
												</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Table head options with primary background end -->
