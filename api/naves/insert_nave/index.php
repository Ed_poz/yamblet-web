<?php 
	
	header('Content-Type: application/json');

	 error_reporting(E_ALL);
	 ini_set("display_errors",1);

	include  '../../common/conn-apiserver.php';
	include  '../../common/conn-medoo.php';
	include  '../../common/functions.php';

	
	$response	=	array(); 
	$id = $_REQUEST["id"];
	$json 		= 	file_get_contents("php://input"); 
	$objeto 	= 	json_decode($json); 

	$HIPERVELOCIDAD = $objeto->hyperdrive_rating;
	$COSTO = $objeto->cost_in_credits;
	$MAX_VELOCIDAD = $objeto->max_atmosphering_speed;

	$database->insert("NAVES", [ 
		"HIPERVELOCIDAD" => $HIPERVELOCIDAD,
		"COSTO" => $COSTO,
		"MAX_VELOCIDAD" => $MAX_VELOCIDAD,
		"ID" => $id
	]);

	valida_error_medoo_and_die(); 

	$response["resultado"]	=	"ok";
		

	print_r(json_encode($response));

?> 