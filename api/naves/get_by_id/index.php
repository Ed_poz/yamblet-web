<?php 

	header('Content-Type: application/json');

	error_reporting(E_ALL);
	ini_set("display_errors",1);


	include  '../../common/conn-apiserver.php';
	include  '../../common/conn-medoo.php';
	include  '../../common/functions.php';

	$id = $_REQUEST["id"];
	$response = array(); 	
	$response = $database->get("NAVES","*",["ID"=>$id]);

	if (is_null($response)) {
		$response="no hay datos";
	}

	valida_error_medoo_and_die(); 
	//print($database->last());
	print_r(json_encode($response)); 

?> 